
rss20_content_importer is a module that complements the  RSS20 parser plugin
of feedapi_eparser. At the moment, this module will download all images in a
the description tag of a feed and replace the src with the path to the local 
copy. This is necessary when the content body has relative image paths 
pointing to its own server.

Please submit a patch/feature request if you have an idea for another
feature.

Installation Instructions:
1. Install the RSS 2.0 Content Importer module
2. Create a feed node with the URL pointing to your RSS 2.0 feed
   URL and the node type of feed item as your content node type. 
   Make sure to check the "Import RSS 2.0 Content" checkbox in 
   the "Feed" field group when creating the feed.
3. Click the Eparser tab of your feed node and select the
   "RSS 2.0" type as your feed type.
4. If you have feedapi_mapper, map the body field to your
   content type body field.
